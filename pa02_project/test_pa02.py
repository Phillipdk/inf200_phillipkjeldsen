# -*- coding: utf-8 -*-

__author__ = 'Dina Rapp, Phillip Krogstrup Kjeldsen'
__email__ = 'drapp@nmbu.no, phikj@nmbu.no'

import chutes_simulation as cs
import random


class TestBoard:
    """ Tests for the class Board"""

    def test_chutes(self):
        """ Test that chutes have a length, i.e. is a list object. """
        b = cs.Board()
        c = b.chutes
        assert len(c) >= 0

    def test_ladder(self):
        """ Test that ladders have a length, i.e. is a list object"""
        b = cs.Board()
        ladders = b.ladders
        assert len(ladders) >= 0

    def test_goal_not_reached(self):
        """ Test that goal_reached returns False before the goal is reached."""
        b = cs.Board()
        g = b.goal_reached(50)
        assert g is False

    def test_position_not_adjust(self):
        """ Test that position_adjustment returns zero when no chute/ladder is
        activated"""
        b = cs.Board()
        assert b.position_adjustment(2) == 0

    def test_position_adjust(self):
        """ Test that position is adjusted when a chute/ladder is activated"""
        b = cs.Board()
        assert b.position_adjustment(1) == 39


class TestPlayer:
    """ Tests for the class Player"""

    def test_player_moves(self):
        """ Checks that the player changes position after move()"""
        b = cs.Board()
        s = cs.Player(b)
        a = s.position
        s.move()
        assert a != s.position

    def test_default_ladders_work(self):
        """ Checks that the players move up the ladder"""
        b = cs.Board()
        s = cs.Player(b)
        random.seed(2)
        s.move()
        assert s.position == 40

    def test_defined_chutes_work(self):
        """ Checks that the player moves down the chutes """
        b = cs.Board(chutes={5: 3})
        s = cs.Player(b)
        random.seed(5)
        s.move()
        assert s.position == 3


class TestResilientPlayer:
    """ Tests for the class ResilientPlayer"""

    def test_chutes_works_resilient(self):
        """ checks that a ResilientPlayer gets extra steps after next move()"""
        b = cs.Board(chutes={5: 2})
        s = cs.ResilientPlayer(b)
        random.seed(5)
        s.move()
        assert s.position == 2
        random.seed(2)
        s.move()
        assert s.position == 4

    def test_extra_steps_resilient(self):
        """ checks that a ResilientPlayer gets extra steps after next move()"""
        b = cs.Board(chutes={5: 2})
        s = cs.ResilientPlayer(b, extra_steps=3)
        random.seed(5)
        s.move()
        assert s.position == 2
        random.seed(2)
        s.move()
        assert s.position == 6


class TestLazyPlayer:
    """ Tests for the class LazyPlayer"""

    def test_ladders_work_lazy(self):
        """ Checks that a LazyPlayer drops a given number of steps after the next move()"""
        b = cs.Board()
        s = cs.LazyPlayer(b)
        random.seed(2)
        s.move()
        assert s.position == 40
        random.seed(5)
        s.move()
        assert s.position == 44

    def test_dropped_steps_non_neg(self):
        """ Checks that the LazyPlayer's step value never is negative """
        b = cs.Board()
        s = cs.LazyPlayer(b, dropped_steps=6)
        random.seed(2)
        s.move()
        assert s.position == 40
        random.seed(5)
        s.move()
        assert s.position == 40


class TestSimulation:
    """ Tests for the Simulation class."""

    def test_single_game(self):
        """ Checks that the correct Player type is returned"""
        s = cs.Simulation([cs.ResilientPlayer], seed=1, randomize_players=True)
        moves, winner = s.single_game()
        assert moves > 0
        assert winner == 'ResilientPlayer'

    def test_single_game_seed(self):
        """Check that single_game works with different seeds. Number of winner moves should be
        different, but the winner type the same"""
        s_1 = cs.Simulation([cs.ResilientPlayer], seed=1)
        moves_1, winner_1 = s_1.single_game()
        s_2 = cs.Simulation([cs.ResilientPlayer], seed=2)
        moves_2, winner_2 = s_2.single_game()

        assert moves_1 != moves_2
        assert winner_1 == winner_2

    def test_run_simulation_no_return(self):
        """ checks the run_simulation doesn't return anything, but saves changes """
        num_games = 3
        s = cs.Simulation(player_field=[cs.ResilientPlayer])

        assert len(s.result_list) == 0
        s.run_simulation(num_games)
        assert len(s.result_list) == num_games
        assert s.run_simulation(2) is None

    def test_get_results_returns_tuple(self):
        """ checks the data type """
        b = cs.Board()
        p = cs.ResilientPlayer(b, extra_steps=2)
        s = cs.Simulation(player_field=[p], board=b, seed=1, randomize_players=True)
        r = s.get_results()
        assert type(r) == list
