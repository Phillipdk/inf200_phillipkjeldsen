# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen', 'Dina Rapp'
__email__ = 'phkj@nmbu.no', 'drapp@nmbu.no'

import random
from datetime import datetime


class Board:
    """
    Maps out the board for the chutes and ladders game.
    Defines default dictionaries for ladders and chutes
    """

    default_ladders = {1: 40, 8: 10, 36: 52, 43: 62, 49: 79, 65: 82, 68: 85}
    default_chutes = {24: 5, 33: 3, 42: 30, 56: 37, 64: 27, 74: 12, 87: 70}

    def __init__(self, ladders=None, chutes=None, goal=90):
        """
        Initialise an instance of a board

        :param ladders: dictionary with from KEY to VALUE
        :param chutes: dictionary with from KEY to VALUE
        :param goal: default is 90 but user input possible
        """
        self.ladder = ladders
        self.chutes = chutes
        self.goal = goal

        if self.ladder:
            self.ladder = self.ladders
        else:
            self.ladders = Board.default_ladders

        if self.chutes:
            self.chutes = self.chutes
        else:
            self.chutes = Board.default_chutes

    def goal_reached(self, position):
        """
        Checks if the player has reached the goal

        :param position: the given players position
        :return: bool: True if the player is at or over the goal position
        """
        return position >= self.goal

    def position_adjustment(self, position):
        """
        Makes sure that the ladders and chutes function is working

        :param position: the position a player has landed at
        :return: number of steps the players need to take (pos or neg) to climb
        a ladder/fall down a chute
        """
        if position in self.chutes:
            return self.chutes[position] - position
        elif position in self.ladders:
            return self.ladders[position] - position
        else:
            return 0


class Player:
    """
    Player class (Super class)
    """

    def __init__(self, board=Board()):
        """
        Initialising a player instance with current position and number of moves
        the player has taken

        :param board: takes in board as an argument
        """
        self.position = 0
        self.board = board
        self.moves = 0

    def move(self):
        """
        makes the player move and adjusts position if a chutes or ladder if the
        players on an event field.
        """
        self.moves += 1
        self.position += random.randint(1, 6)
        self.position += self.board.position_adjustment(self.position)


class ResilientPlayer(Player):
    """
    Resilient Player class (subclass of Player)
    """

    def __init__(self, board=Board(), extra_steps=1):
        """
        Inherits the player attributes and adds extra steps
        :param board: default is to generate a standard board
        :param extra_steps: will be taken if she fall down a chute
        """
        super().__init__(board)
        self.extra_steps = extra_steps
        self.chute_taken = False
        self.board = board

    def move(self):
        """
        Counts moves made by the player
        Makes the Resilient player move
        Remembers if last round had a chute fall and corrects the following move
        """
        self.moves += 1

        if self.chute_taken:
            self.position += random.randint(1, 6) + self.extra_steps
            self.chute_taken = False
        else:
            self.position += random.randint(1, 6)

        adjust = self.board.position_adjustment(self.position)

        if adjust < 0:
            self.position += adjust
            self.chute_taken = True
        else:
            self.position += adjust


class LazyPlayer(Player):
    """
    Lazy Player class (subclass of Player)
    """

    def __init__(self, board=Board(), dropped_steps=1):
        """
        Inherits the player attributes and adds dropped steps
        :param board: default is to generate a standard board
        :param dropped_steps: will not be taken if she climbs a ladder
        """
        super().__init__(board)
        self.dropped_steps = dropped_steps
        self.ladder_taken = False
        self.board = board

    def move(self):
        """
        Counts moves made by the player
        Makes the Lazy player move
        Remembers if she climbed a ladder last round corrects the following move
        She can't move backwards.
        """
        self.moves += 1

        if self.ladder_taken:
            self.position += max(random.randint(1, 6) - self.dropped_steps, 0)
            self.ladder_taken = False
        else:
            self.position += random.randint(1, 6)

        adjust = self.board.position_adjustment(self.position)

        if adjust > 0:
            self.position += adjust
            self.ladder_taken = True
        else:
            self.position += adjust


class Simulation:
    """
    Simulation class
    """
    default_player_field = [Player]

    def __init__(self, player_field=None, board=Board(), seed=12345,
                 randomize_players=True):
        """
        :param player_field: List of which players a participating (player names
        - not instances)
        :param board: standard board is generated
        :param seed: changing random.seed(seed) is possible
        :param randomize_players: bool: True if the player order is to be
        randomized

        defines list and dictionaries used for data collection
        """
        self.player_field = player_field
        if self.player_field:
            self.player_field = self.player_field
        else:
            self.player_field = Simulation.default_player_field

        self.board = board
        random.seed(seed)
        self.randomize_players = randomize_players

        self.result_list = []
        self.player_type = {'Player': 0, 'ResilientPlayer': 0, 'LazyPlayer': 0}
        self.winner_type = {'Player': 0, 'ResilientPlayer': 0, 'LazyPlayer': 0}
        self.duration_type = {'Player': [], 'ResilientPlayer': [], 'LazyPlayer': []}

    def single_game(self):
        """
        Calls instances for the simulation. Counts how many players of each type
        are participating and type of winner.
        :return: number of moves, type of winner and how many player types
        participated
        """

        if self.randomize_players:
            random.shuffle(self.player_field)

        instance_field = [P() for P in self.player_field]

        for player in instance_field:
            self.player_type[type(player).__name__] += 1

        while not any([self.board.goal_reached(player.position) for player in
                       instance_field]):
            for player in instance_field:
                player.move()
                if self.board.goal_reached(player.position):
                    return player.moves, type(player).__name__

    def run_simulation(self, num_games):
        """
        Runs a given number of simulations of the game by calling the
        single_game method. saves result in the simulation object
        :param num_games: number of games in simulation
        :return: nothing
        """

        for i in range(num_games):
            result_tuple = (Simulation.single_game(self))
            self.result_list.append(result_tuple)
            self.winner_type[result_tuple[1]] += 1
            self.duration_type[result_tuple[1]].append(result_tuple[0])

    def get_results(self):
        """
        Returns result_list
        :return: self.result_list
        """
        return self.result_list

    def players_per_type(self):
        """
        Returns how many players per player type participated.
        :return: self.player_type
        """
        return self.player_type

    def winners_per_type(self):
        """
        Returns how many winner there were per player type participating.
        :return: self.winner_type
        """
        return self.winner_type

    def durations_per_type(self):
        """
        Returns how many moves it took each winner to win, sorted by which type
        player.
        :return: self.duration_type
        """
        return self.duration_type


if __name__ == "__main__":
    s = Simulation([Player, ResilientPlayer, LazyPlayer],
                   seed=random.seed(datetime.now()))
    s.run_simulation(30)
    print("Who won? :", s.get_results())
    print("Players per type: ", s.players_per_type())
    print("Winners per type: ", s.winners_per_type())
    print("Game duration per type: ", s.durations_per_type())
