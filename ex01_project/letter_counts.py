# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'


def letter_freq(text):
    letters = [x.lower() for x in text]  # if x.isalpha() == 1]
    d = {}

    for w in letters:
        d[w] = letters.count(w)
    return d

if __name__ == '__main__':
    text = input('Please enter text to analyse: ')
    letter_freq(text)
    frequencies = letter_freq(text)
    for letter, count in frequencies.items():
        print('{:3}{:10}'.format(letter, count))
