# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

from random import randint


def player_guess():
    guess = 0
    while guess < 1:
        guess = int(input('Your guess: '))
    return guess


if __name__ == '__main__':

    guessed = False
    lives = 3
    secret_number = randint(1, 12)
    print('Welcome to Secret Number! Guess the secret number! (from 1 to 12)')

    while not guessed and lives > 0:

        guessed = secret_number == player_guess()  # Evaluating if the guess is
        # correct.

        if not guessed:
            print('Wrong, try again (1 to 12) ! '.format(secret_number))
            lives -= 1

    if lives > 0:
        print('You won {} points.'.format(lives))
    else:
        print('You lost. Correct answer: {}.'.format(secret_number))