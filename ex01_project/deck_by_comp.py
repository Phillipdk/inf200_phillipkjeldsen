# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

SUITS = ('C', 'S', 'H', 'D')
VALUES = range(1, 14)


# this double loop "creates" the card stock
def deck_loop():
    deck = []
    for suit in SUITS:
        for val in VALUES:
            deck.append((suit, val))
    return deck


def deck_comp():
    """
    creates the card stock
    :return: list with ['C',1]
    """
    return[(suit, val) for suit in SUITS for val in VALUES ]


if __name__ == '__main__':
    if deck_loop() != deck_comp():
        print('ERROR!')
