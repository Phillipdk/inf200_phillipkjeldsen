# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'


def squares_by_loop(n):
    k = []
    for i in range(n):
        if i % 3 == 1:
            k.append(i**2)
    return k

def squares_by_comp(n):
    return[k**2 for k in range(n) if k % 3 == 1]

if __name__ == '__main__':
    n = 5
    if squares_by_comp(n) != squares_by_loop(n):
        print('ERROR!')
    if squares_by_comp(n) == squares_by_loop(n):
        print('\nGreat success!!')
