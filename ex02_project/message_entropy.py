# -*- coding: utf-8 -*-

'''
Dette program regner bogstav entropien i en tekst-fil
Kommentarer er en alternativ løsning, der kan give mere data.
'''

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

import  math

def entropy(message):
    letters = [ord(x) for x in message]
    d = {}
    p = {}
    #H = [0] * 256
    H = 0
    for w in range(256):
        d[w] = 0
        p[w] = 0
        #H[w] = 0

    for w in letters:
        d[w] = letters.count(w)

    for w in d:
        if d[w] > 0:
            p[w] = d[w]/sum(d.values())
            #H[w] = -p[w] * math.log(p[w],2)
            H = H - p[w] * math.log(p[w],2)
    #H = - Sum\_i p log\_2 p
    #H = sum(H)

    return H

if __name__ == "__main__":
    for msg in '', 'aaaa', 'aaba', 'abcd', 'This is a short text.':
        print('{:25}: {:8.3f} bits'.format(msg, entropy(msg)))