# -*- coding: utf-8 -*-

'''
Dette program antal bogstav i en tekst-fil
'''

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'


def char_counts(filename):
    file = open(filename, 'r').read() # encoding = 'uft-8' ?
    letters = [ord(x) for x in file]
    d = {}

    for w in range(256):
        d[w] = 0

    for w in letters:
        d[w] = letters.count(w)

    return d


if __name__ == '__main__':

    filename = 'textfilename.txt'
    frequencies = char_counts(filename)
    for code in range(256):
        if frequencies[code] > 0:
            print('{:3}{:>4}{:6}'.format(code,
                                         chr(code) if code >= 32 else '',
                                         frequencies[code]))