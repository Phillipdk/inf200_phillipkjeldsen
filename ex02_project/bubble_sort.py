# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Sorting lists of numbers
Kommentarer er en alternativ løsning
'''


def bubble_sort(data):

    data_new = list(data)

    for i in range(len(data)):

        for j in range(len(data)-1): # Muligvis brug en counter til at optimere?
            data = data_new

            if data[j] > data[j+1]:
                #data_new.append(data[i+1])
                data_new[j:j+2] = reversed(data[j:j+2])

    return data_new

    #return sorted(data, key=int)


if __name__ == "__main__":

    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print('{!s:>15} --> {!s:>15}'.format(data, bubble_sort(data)))