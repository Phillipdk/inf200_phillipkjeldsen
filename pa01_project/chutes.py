# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen', 'Dina Rapp'
__email__ = 'phkj@nmbu.no', 'drapp@nmbu.no'

import random
import numpy as np


'''
Chutes and ladders game. 
'''


def make_ladders():
    """
    makes ladders for the game
    :return:
    """
    ladders = {1: 40,
               8: 10,
               36: 52,
               43: 62,
               49: 79,
               65: 82,
               68: 85}
    return ladders


def make_chutes():
    """
    makes chutes for the game
    :return:
    """
    chutes = {24: 5,
              33: 3,
              42: 30,
              56: 37,
              63: 27,
              74: 12,
              87: 70}
    return chutes


def roll_the_dice():
    """
    simulates a die
    :return:
    """
    die = random.randint(1, 6)
    return die


def single_game(num_players):
    """
    Returns duration of single game.

    :param num_players: number of players in game
    :return: number of moves the winning player needed to reach the goal
    """

    global winner_moves
    positions = np.zeros(num_players)
    players = list(range(num_players))

    player_positions = dict(zip(players, positions))
    chutes = make_chutes()
    ladders = make_ladders()
    moves = 0
    moves_list = []

    for i in range(num_players):
        while player_positions[i] < 90:
            die = roll_the_dice()
            player_positions[i] = player_positions[i] + die
            moves += 1

            if player_positions[i] in chutes:
                player_positions[i] = chutes[player_positions[i]]

            elif player_positions[i] in ladders:
                player_positions[i] = ladders[player_positions[i]]

        moves_list.append(moves)
        winner_moves = min(moves_list)

    # print(moves_list)
    # print(player_positions)

    return winner_moves


def multiple_games(num_games, num_players):
    """
    Returns durations of a number of games.

    :param num_games: number of games to play
    :param num_players: number of players in each game
    :return: sequence with number of moves needed in each game
    """
    winner_moves = []
    for _ in range(num_games):
        winner_moves.append(single_game(num_players))

    return winner_moves


def multi_game_experiment(num_games, num_players, seed):
    """
    Returns durations of a number of games when playing with given seed.

    :param num_games: number of games to play
    :param num_players: number of players in each game
    :param seed: random number seed for experiment
    :return: sequence with number of moves needed in each game
    """

    random.seed(seed)
    winner_moves = []

    for _ in range(num_games):
        winner_moves.append(single_game(num_players))

    return winner_moves


if __name__ == "__main__":
    num_games = 100
    num_players = 4
    seed = 134952347523452832489637458

    game_time = multi_game_experiment(num_games, num_players, seed)

    # print('Winner times are: ', game_time)
    print('The shortest game was: ', min(game_time), ' moves')
    print('The longest game was: ', max(game_time), ' moves')
    print('The median game time is:', np.median(game_time), 'moves')
    print('Average game time is: ', np.mean(game_time))
    print('Standard deviation is: ', np.std(game_time))
