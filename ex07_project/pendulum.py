# -*- coding: utf-8 -*-

import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
A simulation of a double pendulum
'''

g = 9.81


def dydt(y, t, m, l):
    """
    ODEs that determine the position
    :param y: initial angles and momentum ([theta_1, theta_2, p_theta_1, p_theta_2])
    :param t: time
    :param m: mass
    :param l: length of rods
    :return: the time derivatives of the input
    """
    dtheta_1 = 6 / (m * l ** 2) * (2 * y[2] - 3 * np.cos(y[0] - y[1]) * y[3]) / (16 - 9 * (np.cos(y[0] - y[1])) ** 2)
    dtheta_2 = 6 / (m * l ** 2) * (8 * y[3] - 3 * np.cos(y[0] - y[1]) * y[2]) / (16 - 9 * (np.cos(y[0] - y[1])) ** 2)

    dp_theta_1 = - 1 / 2 * m * l ** 2 * (dtheta_1 * dtheta_2 * np.sin(y[0] - y[1]) + 3 * g / l * np.sin(y[0]))
    dp_theta_2 = - 1 / 2 * m * l ** 2 * (-dtheta_1 * dtheta_2 * np.sin(y[0] - y[2]) + g / l * np.sin(y[1]))

    dy = np.array([dtheta_1, dtheta_2, dp_theta_1, dp_theta_2])

    return dy


def psim(theta_1, theta_2, m, l, dt, t_max):
    """
    Integrates the input and determines the angles and momentums
    :param theta_1: start angle between gravitational force direction and rod 1
    :param theta_2: start angle between gravitational force direction and rod 2
    :param m: mass
    :param l: length
    :param dt: increment steps
    :param t_max: simulation time
    :return: the angles and momentum at the given increments
    """
    y0 = np.array([theta_1, theta_2, 0, 0])
    t = np.array(np.linspace(0, t_max, t_max // dt + 1))

    ode_sol = scipy.integrate.odeint(dydt, y0, t, args=(m, l))
    sol = np.insert(ode_sol, 0, t, axis=1)

    return sol


def plot_psim(theta_1, theta_2, m, l, dt, t_max):
    """
    Plots the calculated trajectories.
    :param theta_1: initial angles for rod 1
    :param theta_2: initial angles for rod 2
    :param m: mass
    :param l: length
    :param dt: time increments
    :param t_max: total simulation time
    :return: The figure
    """
    plot = psim(theta_1, theta_2, m, l, dt, t_max)

    x1 = [l / 2 * np.sin(plot[i][0]) for i in range(len(plot))]
    y1 = [-l / 2 * np.cos(plot[i][0]) for i in range(len(plot))]
    x2 = [l * (np.sin(plot[i][0]) + np.sin(plot[i][1]) / 2) for i in range(len(plot))]
    y2 = [-l * (np.cos(plot[i][0]) + np.cos(plot[i][1]) / 2) for i in range(len(plot))]

    plt.plot(x1, y1, label='Rod 1 centre')
    plt.plot(x2, y2, label='Rod 2 centre')
    plt.axis([-2 * l, 2 * l, -2 * l, 2 * l])
    plt.title('Projectory of a double pendulum')
    plt.xlabel('x position')
    plt.ylabel('y position')
    plt.legend()
    plt.grid(True)

    return plt.figure()
