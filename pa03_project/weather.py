# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''

'''


def load_data(filename, from_year, to_year):
    data = pd.DataFrame()
    from_year, to_year = from_year - 2003, to_year - 2003
    try:
        data = pd.read_excel(filename, sheet_name=list(range(from_year, to_year + 1)))

    except ValueError as e:
        print('Loading failed:', e)

    all_data = pd.DataFrame()

    for i in data:
        di = data[i]
        all_data = all_data.append(di)

    all_data.columns = ['DATE', 'TAM', 'TAN', 'TAX', 'RR', 'RRM', 'GLOB', 'UV', 'TG']
    all_data['DATE'] = pd.to_datetime(all_data['DATE'])

    return all_data


def plot_temperature(input_data):
    """ Creates a plot of the weekly temperature"""

    input_data = input_data[['DATE', 'TAM']]
    input_data = input_data.set_index('DATE')
    input_data = input_data.TAM.resample('W').mean()

    input_data['doy'] = input_data.index.dayofyear
    input_data['year'] = input_data.index.year

    p_input_data = pd.pivot_table(input_data, index=['doy'], columns=['year'])
    print(p_input_data)


    print(input_data_W)

    plot = input_data_W.plot(title='Weekly Average Temperature i Ås',
                                  grid=True,
                                  legend=True,
                                  style=['green', 'blue', 'red'],
                                  figsize=(6, 6))
    plt.ylabel('Temperatur i Celsius')

    return plt.Figure(plt.show(plot))


if __name__ == '__main__':
    filename = 'Aas dogn 2003-2012 Corrected.xlsx'
    loaded_data = load_data(filename, 2004, 2006)
    plot_temperature(loaded_data)
