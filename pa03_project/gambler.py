# -*- coding: utf-8 -*-

import random as r

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Illustrate Gambler's ruin stochastic process.
'''


def propagator(M, p):
    pass


def init_state(M, m_0):
    pass


def evolve(T, q, n):
    pass


def t_win(Q):
    pass


def t_lose(Q):
    pass


def plot_analysis(M, m_0, p, n):
    pass
