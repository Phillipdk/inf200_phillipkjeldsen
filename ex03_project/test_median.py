# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Testing file to see in median() works properly
'''


def median(data):
    """
    Returns median of data.

    :param data: An iterable of containing numbers
    :return: Median of data
    """
    if not data:
        return False

    sdata = sorted(data)
    n = len(sdata)
    return (sdata[n//2] if n % 2 == 1
            else 0.5 * (sdata[n//2 - 1] + sdata[n//2]))


def test_one_element_list():
    """ Checks that the funtions works for a single element list"""
    assert median([1]) == 1


def test_odd_numbers():
    """ Tests that median() works for odd numbers """
    assert median([2, 4, 6]) == 4


def test_even_numbers():
    """ Tests that median() works for even numbers """
    assert median([1, 3, 5, 7]) == 4


def test_ordered_numbers():
    """ Tests that median() works for ordered numbers """
    assert median([1, 2, 3, 4, 5]) == 3


def test_reverse_ordered_numbers():
    """ Tests that median() works for reverse orderes numbers """
    assert median([5, 4, 3, 2, 1]) == 3


def test_unordered_numbers():
    """ Tests the median() works for unordered numbers """
    assert median([3, 5, 4, 1, 2]) == 3


def test_no_empty():
    """Tests that median() must have a list when a length """
    assert median([]) is False, 'You can not insert a 0 element list/tuple'


def test_original_data_unchaged():
    """ Makes sure that the original list is unchanced"""
    data = [4, 1, 5, 2, 3]
    original_data = data
    assert median(data) == 3
    assert original_data == data, 'Data skal ikke ændres undervejs i programmet'


def test_works_for_tuples():
    """ Tests that the result for a list and tuple is the same"""
    data_tup = (4, 1, 5, 2, 3)
    data_list = list(data_tup)
    assert median(data_tup) == median(data_list), 'Programmet virker ikke for tuples'
