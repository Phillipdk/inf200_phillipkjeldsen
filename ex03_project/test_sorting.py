# -*- coding: utf-8 -*-

import random as r

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Testing bubble_sort() programmet
'''


def bubble_sort(in_data):
    s_data = list(in_data)
    for j in reversed(range(len(s_data))):
        for k in range(j):
            if s_data[k+1] < s_data[k]:
                s_data[k], s_data[k+1] = s_data[k+1], s_data[k]
    return s_data


def test_empty():
    """Test that the sorting function works for empty list"""
    assert bubble_sort([]) == sorted([], key=int)


def test_single():
    """Test that the sorting function works for single-element list"""
    assert bubble_sort(["1"]) == sorted(["1"], key=int)


def test_sorted_is_not_original():
    """
    Test that the sorting function returns a new object.

    Consider

    data = [3, 2, 1]
    sorted_data = bubble_sort(data)

    Now sorted_data shall be a different object than data,
    not just another name for the same object.
    """

    data = [5,4,3,2]

    assert id(data) != id(bubble_sort(data))


def test_original_unchanged():
    """
    Test that sorting leaves the original data unchanged.

    Consider

    data = [3, 2, 1]
    sorted_data = bubble_sort(data)

    Now data shall still contain [3, 2, 1].
    """
    data = [5,4,3,2]
    original_data = data
    assert bubble_sort(data) == sorted(data, key =int)
    assert data == original_data


def test_sort_sorted():
    """Test that sorting works on sorted data."""
    sorted_data = (1, 2, 3, 4)
    assert bubble_sort(sorted_data) == list(sorted_data)


def test_sort_reversed():
    """Test that sorting works on reverse-sorted data."""
    data = [7, 6, 5, 4, 3, 2, 1]
    assert bubble_sort(data) == data[::-1] #reverses


def test_sort_all_equal():
    """Test that sorting handles data with identical elements."""
    data = [1, 1, 1, 1]
    assert bubble_sort(data) == data


def test_sorting():
    """
    Test sorting for various test cases.

    This test case should test sorting of a range of data sets and
    ensure that they are sorted correctly. These could be lists of
    numbers of different length or lists of strings.
    """
    #r.seed(311291)  brug evt et seed. Det er vigtigt det er ens fødselsdag. Ellers crasher hele internettet ;)
    random_data = r.sample(range(r.randint(-100, 0), r.randint(0, 100)), r.randint(4, 10))
    assert bubble_sort(random_data) == sorted(random_data, key=int)

    data = ['4', '2', '36', '12']
    assert bubble_sort(data) == sorted(data,key=str) #key kan ikke defineres som int.


