# -*- coding: utf-8 -*-

import random as r

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
This describes how difficult it is to get home drunk
'''


class Walker:
    def __init__(self, position, home):
        self.position = position
        self.home = home
        self.steps = 0

    def move(self):
        step = r.randint(0, 1)
        if step == 0:
            self.position = self.position - 1
        elif step == 1:
            self.position = self.position + 1

        self.steps += 1
        return

    def is_at_home(self):
        if Walker.get_position(self) == self.home:
            return True

    def get_position(self):
        return self.position

    def get_steps(self):
        return self.steps


if __name__ == "__main__":
    for i in [1, 2, 5, 10, 20, 50, 100]:
        history = []
        for j in range(5):
            walker = Walker(0, i)
            while not walker.is_at_home():
                walker.move()
                walker.is_at_home()

                if walker.is_at_home():
                    # print(walker.get_steps())
                    history.append(walker.get_steps())

        print('Distance: {:3} ->  Path Lengths: {}'.format(i, sorted(history)))


    def test_empty():
        """Test that the sorting function works for empty list"""
        assert ([]) == sorted([], key=int)
