# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Random number generators
'''


class LCGRand:
    """
    Random number generator

    :param: seed
    :returns: new random number based on the previous
    """

    def __init__(self, seed):
        self.seed = [seed]
        self.n = 0

    def rand(self):
        """
        :param: takes a seed as a initial input
        :return: a modified random number based on the previous on
        """
        r = self.seed
        a = 7**5
        m = 2**31 - 1
        r.append(a * r[self.n] % m)
        self.n += 1
        return r[self.n]


class ListRand:
    """
    Random number generator

    :param: a list of random numbers
    :return: a new element of the list every time it's called
    """
    def __init__(self, my_list):
        self.my_list = my_list
        self.index = len(my_list)
        self.n = 0

    def rand(self):

        if self.n >= self.index:
            raise RuntimeError('The list of random numbers is empty.')
        else:
            value = self.my_list[self.n]
            self.n += 1

        return value

if __name__ == "__main__":
    """
    Defines a seed and uses the list made by the first class
    as an input to the second class
    
    """
    seed = 346
    my_list = [seed]
    numbers_to_generate = 3

    LCG = LCGRand(seed)
    for _ in range(numbers_to_generate):
        my_list.append(LCG.rand())
    print(my_list)

    ListRand = ListRand(my_list)
    for _ in range(len(my_list)):
        print(ListRand.rand())


