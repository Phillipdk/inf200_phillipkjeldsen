# -*- coding: utf-8 -*-


__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Using a decorator to access a functionality. Here it's presenting the calculation process. 
'''


def log_in_out(original_function):
    """ accessing the wrapper function """
    def wrapper_function(x):
        """ showing it all goes through here """
        print('Argument:', x)
        print('Result:', original_function(x))
        return original_function(x)
    return wrapper_function


@log_in_out
def sqr(x):
    """ decorator function """
    return x*x


y = sqr(5)
print('Returned: ', y)
