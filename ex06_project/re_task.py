# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Different string manipulations
'''


def select_int(filename):
    """
    :param filename: input file lines of strings
    :return: a list of the lines with letters 'int'
    """

    result = []
    with open(filename) as input_file:
        for line in input_file:
            if 'int' in line:
                result.append(line.strip())
    return result


def select_at(filename):
    """
    :param filename: input file lines of strings
    :return: a list of the lines with the work 'at'
    """
    result = []
    with open(filename) as input_file:
        for line in input_file:
            if 'at' in line.lower().split():
                result.append(line.strip())
    return result


def upper_cat(filename):
    """
    :param filename: input file lines of strings
    :return: a list of the lines with the letters 'cat' replaces with 'CAT'
    """
    result = []
    with open(filename) as input_file:
        for line in input_file:
            result.append(str(line).replace('cat', 'CAT').strip())
    return result


if __name__ == "__main__":
    file = 'test_int.txt'
    print(select_int(file))
    file = 'test_at.txt'
    print(select_at(file))
    file = 'test_cat.txt'
    print(upper_cat(file))
