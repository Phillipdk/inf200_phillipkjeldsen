# -*- coding: utf-8 -*-

"""
Minimal tests for EX06.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

import pytest

from re_task import select_int, select_at, upper_cat
from decorate import log_in_out
from ran_gen import list_rng, lcg_rng


class TestRE:
    """
    Tests for Task A.
    """

    def test_select_int(self):
        res = select_int('test_int.txt')
        assert res == ['Printed material is', 'internationally as we know.']

    def test_select_at(self):
        res = select_at('test_at.txt')
        assert res == ['at many congresses, much is discussed.',
                       'At home, we rest.']

    def test_upper_cat(self):
        res = upper_cat('test_cat.txt')
        assert res == ['My cute CAT', 'was today', 'conCATenated.',
                       'But can CATs conCATenate?']


class TestDecorator:
    """
    Tests for Task B.
    """

    def test_decorator(self, capsys):
        @log_in_out
        def g(x):
            return x**3

        assert g(4) == 64
        out, err = capsys.readouterr()
        assert out == 'Argument: 4\nResult: 64\n'


class TestRNG:
    """
    Tests for Task C.
    """

    def test_list_rng(self):
        lr = list_rng([10, 12])
        assert next(lr) == 10
        assert next(lr) == 12
        with pytest.raises(StopIteration):
            next(lr)

    def test_list_rng_list(self):
        values = [3.4, 6.7, 2.1]
        lr = list_rng(values)
        rn = [n for n in lr]
        assert rn == values

    def test_lcg(self):
        cg0 = lcg_rng(0)
        cg1 = lcg_rng(1)
        assert next(cg0) == 0
        assert next(cg1) == 16807

    def test_lcg_list(self):
        cg = lcg_rng(10)
        values = [next(cg) for _ in range(100)]
        assert len(values) == 100
