# -*- coding: utf-8 -*-


__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

"""
Two different random number generators made with python generators. 
"""


def list_rng(input_list):
    """
    :param input_list: a list of random numbers
    :return: the next number in the list. StopIteration if list is exhausted
    """
    for i in range(len(input_list)):
        yield input_list[i]


def lcg_rng(seed):
    """
    :param seed: start seed
    :return: returns a new pseudo-random number from the given equation everytime it's requested
    """
    while True:
        r = seed
        a = 7 ** 5
        m = 2 ** 31 - 1
        r = a * r % m
        yield r
