# -*- coding: utf-8 -*-

import random as r

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Simulation of how difficult it is to get home drunk
'''


class Walker:
    def __init__(self, position, home):
        """
        :param position: position of the walker (starts a 0)
        :param home: position to the walker's home
        """
        self.position = position
        self.home = home
        self.steps = 0

    def move(self):
        """
        Change the position by +1 og -1 by equal probability
        """
        self.position = self.position + (2 * r.randint(0, 1) - 1)
        self.steps += 1

    def is_at_home(self):
        """ Returns TRUE if the walker is home """
        return self.get_position() == self.home

    def get_position(self):
        """ Gives the position of the walker"""
        return self.position

    def get_steps(self):
        """ Returns the numbers of steps taken by the walker"""
        return self.steps


class Simulation:
    def __init__(self, start, home, seed):
        """
        :param start: walkers's initial position
        :param home: walk ends when walker reaches home
        :param seed: random generator seed
        """
        self.position = start
        self.home = home
        r.seed(seed)
        self.history = []

    def single_walk(self):
        """ Simulate single walk from start to home, returning number of steps

        :returns: number of steps taken
        """
        walker = Walker(self.position, self.home)
        while not walker.is_at_home():
            walker.move()
        return walker.get_steps()

    def run_simulation(self, num_walks):
        """
        Run a set of walks, returns list
        :param num_walks:
        :return:
        """
        self.history = []
        for _ in range(num_walks):
            self.history.append(self.single_walk())
        return self.history


if __name__ == "__main__":
    """
    The main section shall simulate: 
    20 walks from start 0 to home 10
    20 walks from start 10 to home 0
    Each of these twice for seed = 12345
    and once for seed = 54321
    Print results
    """

    number_of_walks = 20

    for start_value, home_value in [0, 10], [10, 0]:
        for seed_input in [12345, 12345, 54321]:
            simulate = Simulation(start_value, home_value, seed_input)
            print(start_value, "->", home_value, "Seed =", seed_input, simulate.run_simulation(number_of_walks))
