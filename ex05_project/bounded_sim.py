# -*- coding: utf-8 -*-

__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Simulates a walker going forward or backwards with a 50/50 chance and prints how steps it takes him to get home
If he goes 0, -10, -100, -1000, or -10000 steps backwards he is stopped.
'''

from walker_sim import Walker, Simulation


class BoundedWalker(Walker):

    def __init__(self, position, home, left_limit, right_limit):
        """
        :param position: initial position of the walker
        :param home: position of the walker's home
        :param left_limit: left boundary of walker's movement
        :param right_limit: right boundary of walker's movement
        """
        Walker.__init__(self, position, home)
        self.left_limit = left_limit
        self.right_limit = right_limit

    def check_limits(self):
        """ Checks is the walker moves into a limit and cancels the move if the limit is reached
        self.position = self.limit er en mulig optimering """
        if self.position > self.right_limit:
            self.position -= 1
            self.steps -= 1
        elif self.position < self.left_limit:
            self.position += 1
            self.steps -= 1


class BoundedSimulation(Simulation):
    def __init__(self, position, home, seed, left_limit, right_limit):
        """
        :param position: start position of walker
        :param home: Home position
        :param seed: seed to random.seed(seed)
        :param left_limit: stops the walker from walking backwards to far
        :param right_limit: stop the walker from walker to far (in case he could take 2 steps at once)
        """
        Simulation.__init__(self, position, home, seed)
        self.left_limit = left_limit
        self.right_limit = right_limit

    def bounded_single_walk(self):
        """ Simulates a single walk where we check for boundaries """
        bounded_walker = BoundedWalker(self.position, self.home, self.left_limit, self.right_limit)
        while not bounded_walker.is_at_home():
            bounded_walker.move()
            bounded_walker.check_limits()
        return bounded_walker.get_steps()

    def bounded_run_simulation(self, num_walks):
        """
        Run a set of walks, returns list
        :param num_walks: number of interations
        :return: list of how many
        """
        self.history = []
        for _ in range(num_walks):
            self.history.append(self.bounded_single_walk())
        return self.history


if __name__ == "__main__":

    start_input = 0
    home_input = 20
    num_walks_input = 20
    seed_input = 12345
    right_limit_input = 20
    history = []
    for left_limit_input in [0, -10, -100, -1000, -10000]:
        bounded_simulation = BoundedSimulation(start_input, home_input, seed_input, left_limit_input, right_limit_input)
        result_list = bounded_simulation.bounded_run_simulation(num_walks_input)
        print("SUM:", sum(result_list), "Left limit is set to:", left_limit_input, result_list)
