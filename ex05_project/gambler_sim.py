# -*- coding: utf-8 -*-

import random


__author__ = 'Phillip Kjeldsen'
__email__ = 'phkj@nmbu.no'

'''
Simulating a game called Gambler's ruin
'''


class Gambler:
    """
    Defines the gambling functions
    """

    def __init__(self, initial, total, p):
        """
        :param initial: The money that the gambler first has
        :param total: the total amount of money gambler + bank
        :param p: probability of winning
        """
        self.gamblers_money = initial
        self.total = total
        self.p = p
        self.coins_flipped = 0

    def is_broke(self):
        """ checks if the gambler is broke and returns TRUE"""
        if self.gamblers_money == 0:
            # print("Gambler lost all")
            return True
        else:
            return False

    def owns_all(self):
        """ checks if the gambler has all the money and returns TRUE"""
        if self.gamblers_money == self.total:
            # print("Gambler owns all")
            return True
        else:
            return False

    def play(self):
        """ Plays a round flip a coin"""
        if random.random() > self.p:
            self.gamblers_money += 1
            # print("Gambler wins a coin")
        else:
            self.gamblers_money -= 1
            # print("Gambler looses a coin")


class GamblerSimulation(Gambler):
    """
    Defines the simulations
    """

    def __init__(self, initial, total, p, seed):
        """
        :param initial:
        :param total:
        :param p:
        :param seed:
        """
        Gambler.__init__(self, initial, total, p)
        random.seed(seed)

    def single_game(self):
        gambler = Gambler(self.gamblers_money, self.total, self.p)
        while not gambler.is_broke() and not gambler.owns_all():
            gambler.play()
            self.coins_flipped += 1
        return gambler.owns_all(), self.coins_flipped

    def run_simulation(self, num_games):
        list_of_bank_wins, list_of_gambler_wins = [], []

        for _ in range(num_games):
            gambler_wins, self.coins_flipped = self.single_game()
            if gambler_wins:
                list_of_gambler_wins.append(self.coins_flipped)
            elif not gambler_wins:
                list_of_bank_wins.append(self.coins_flipped)
        return list_of_gambler_wins, list_of_bank_wins


if __name__ == "__main__":
    m = 25
    M = 100
    num_games_input = 20
    seed_input = 12
    for p_input in [0.0, 0.1, 0.2, 0.4, 0.45, 0.49, 0.5, 0.9]:
        gambler_sim = GamblerSimulation(m, M, p_input, seed_input)
        gambler_win, bank_win = gambler_sim.run_simulation(num_games_input)
        print("p:", p_input, " - >", "Gambler wins:", len(gambler_win), "Bank wins:", len(bank_win))